#pragma once
#include <iostream>

namespace ribomation::account {
    using namespace std;

    class Account {
        double balance = 0;
        float  rate    = 2;

    public:
        Account(double balance = 0, float rate = 2) : balance{balance}, rate{rate} {
            cout << "Account{" << balance << ", " << rate << "} @ " << this << "\n";
        }

        ~Account() {
            cout << "~Account{} @ " << this << "\n";
        }

        Account(const Account& that) : balance{that.balance}, rate{that.rate} {
            cout << "Account{const Account&: " << balance << ", " << rate << "} @ " << this << "\n";
        }

        Account(Account&& that) : balance{that.balance}, rate{that.rate} {
            cout << "Account{Account&&: " << balance << ", " << rate << "} @ " << this << "\n";
        }

        Account& operator=(const Account&) = delete;
        Account& operator=(Account&&) = delete;

        double value() const {
            return balance;
        }

        double increase() {
            balance *= 1 + rate / 100;
            return balance;
        }

        double decrease() {
            balance /= 1 + rate / 100;
            return balance;
        }

        friend ostream& operator<<(ostream& os, const Account& a) {
            return os << "Account{SEK " << a.balance << ", " << a.rate << "%}";
        }
    };

    inline Account& operator++(Account& a) {
        a.increase();
        return a;
    }

    inline Account& operator--(Account& a) {
        a.decrease();
        return a;
    }

}

