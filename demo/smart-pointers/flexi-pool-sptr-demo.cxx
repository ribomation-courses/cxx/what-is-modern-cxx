#include <iostream>
#include <memory>
#include "flexi-pool.hxx"
#include "account.hxx"

using namespace std;
using namespace std::literals;
using namespace ribomation::memory;
using namespace ribomation::account;

SmartPtr<Account> use(SmartPtr<Account> q) {
    cout << "[use]   q: : " << *q << "\n";
    cout << "[use] --q: : " << --*q << "\n";
    return q;
}

int main() {
    byte          memory[10 * sizeof(Account)];
    Pool<Account> pool{memory, sizeof(memory)};
    cout << "pool.size: " << pool.size() << "\n";
    {
        auto ptr = mkPtr(pool, 1000.0, 2.75F);
        cout << "pool.size: " << pool.size() << "\n";
        cout << "  ptr: " << *ptr << "\n";
        cout << "++ptr: " << ++*ptr << "\n";

        cout << "------\n";
        auto p2 = move(ptr);
        cout << "  p2 : " << *p2 << "\n";
        cout << "  ptr: " << ptr.get() << "\n";

        cout << "------\n";
        ptr = use(move(p2));
        cout << "  ptr: " << *ptr << "\n";
        cout << "  p2 : " << p2.get() << "\n";
    }
    cout << "pool.size: " << pool.size() << "\n";
    return 0;
}

