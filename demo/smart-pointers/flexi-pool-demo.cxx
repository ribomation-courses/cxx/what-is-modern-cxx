#include <iostream>
#include "flexi-pool.hxx"
#include "account.hxx"

using namespace std;
using namespace std::literals;
using namespace ribomation::memory;
using namespace ribomation::account;


void use(Pool<Account>& pool) {
    Account* a = new (pool.alloc()) Account{1000, 2.5};
    cout << "  a: " << *a << "\n";
    cout << "++a: " << ++*a << "\n";
    cout << "--a: " << --*a << "\n";
    pool.dispose(a);
}

int main() {
//    byte memory[10 * sizeof(Account)];
//    Pool<Account> pool{memory, sizeof(memory)};
    const unsigned SIZE = 10 * sizeof(Account);
    Pool<Account> pool{reinterpret_cast<byte*>(alloca(SIZE)), SIZE};

    use(pool);
    return 0;
}

