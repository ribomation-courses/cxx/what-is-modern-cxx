What is Modern C++ (2h seminar)
====

Here you can find a PDF with the slides and some of the demo programs.

Build Demo Programs
----

You need the modern C++ compiler (such as GCC version 8) to build the demo programs.

Here are instructions for building a demo program on Ubuntu Linux.

First you need to have GCC/G++ version 8 installed.

    sudo apt install g++-8

Then, you need the build tools

    sudo apt install make cmake

Finally, change directiry into the target folder and run the following

    mkdir -p build && cd $_
    cmake ..
    make

There you now be one (or more) executable(s) in the `build` directory.

***
*If you have any questions, don't hesitate to contact me*<br>
**Jens Riboe**<br/>
Ribomation AB<br/>
[jens.riboe@ribomation.se](mailto:jens.riboe@ribomation.se)<br/>
[www.ribomation.se](https://www.ribomation.se)<br/>

